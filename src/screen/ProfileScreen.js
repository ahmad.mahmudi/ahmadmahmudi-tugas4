import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from 'react-native';
import React from 'react';

export default function ProfileScreen({navigation}) {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.card1}>
          <Image
            source={require('../assetss/image/fotoprofil.png')}
            style={styles.foto}
          />
          <Text style={styles.nama}>Agil Bani</Text>
          <Text style={styles.email}>gilagil@gmail.com</Text>
          <TouchableOpacity
            style={styles.editbtn}
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'Edit Profile'})
            }>
            <Text style={styles.editbtntext}>Edit</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.card2}>
          <TouchableOpacity style={styles.settingbtn}>
            <Text style={styles.settingtext}>About</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.settingbtn}>
            <Text style={styles.settingtext}>Terms & Condition</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.settingbtn}
            onPress={() => {
              navigation.navigate('ProfileNavigation',{screen: 'FAQ'});
            }}>
            <Text style={styles.settingtext}>FAQ</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.settingbtn}>
            <Text style={styles.settingtext}>History</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.settingbtn}>
            <Text style={styles.settingtext}>Setting</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.card3}>
          <TouchableOpacity
            style={styles.logoutbtn}
            onPress={() => {
              navigation.navigate('AuthNavigation');
            }}>
            <Image
              source={require('../assetss/icon/LogOut.png')}
              style={styles.logoutimg}
            />
            <Text style={styles.logouttext}>Log Out</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  card1: {
    width: '100%',
    height: 295,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  foto: {
    width: 95,
    height: 95,
  },
  nama: {
    color: '#050152',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 5.5,
  },
  email: {
    color: '#A8A8A8',
    fontSize: 10,
    fontWeight: '500',
  },
  editbtn: {
    width: 100,
    height: 28,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F8FF',
    borderRadius: 18,
    marginTop: 22,
  },
  editbtntext: {
    color: '#050152',
    fontWeight: 'bold',
    fontSize: 14,
  },
  card2: {
    width: '95%',
    height: 295,
    backgroundColor: 'white',
    alignSelf: 'center',
    marginTop: 12,
    paddingLeft: 80,
  },
  settingbtn: {
    height: '20%',
    justifyContent: 'center',
  },
  settingtext: {
    color: 'black',
    fontSize: 16,
    fontWeight: '500',
  },
  card3: {
    width: '95%',
    height: 50,
    marginTop: 19,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  logoutbtn: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    alignSelf: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoutimg: {
    width: 24,
    height: 24,
  },
  logouttext: {
    color: '#EA3D3D',
    fontSize: 16,
    fontWeight: '500',
  },
});
