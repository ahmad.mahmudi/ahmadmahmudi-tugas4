import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import React from 'react';

const DATA = [
  {
    id: '1',
    title: 'Bank Transfer',
    image: require('../assetss/icon/Transfer.png'),
  },
  {
    id: '2',
    title: 'OVO',
    image: require('../assetss/icon/OVO.png'),
  },
  {
    id: '3',
    title: 'Kredit',
    image: require('../assetss/icon/Transfer.png'),
  },
];

const Item = ({title, image}) => (
  <View style={styles.button}>
    <Image source={image} style={styles.transferimg} />
    <Text style={styles.button1}>{title}</Text>
  </View>
);
export default function Checkout({navigation}) {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.card1}>
          <Text style={styles.title}>Data Customer</Text>
          <Text style={styles.nama}>Agil Bani (08137663476)</Text>
          <Text style={styles.alamat}>
            Jl. Perumnas, Condong Catur, Sleman, Yogyakarta
          </Text>
          <Text style={styles.email}>gantengdoang@dipanggan.com</Text>
        </View>
        <View style={styles.card2}>
          <Text style={styles.title}>Alamat Outlet Tujuan</Text>
          <Text style={styles.namatoko}>
            Jack Repair - Seturan (021-343457)
          </Text>
          <Text style={styles.alamattoko}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View style={styles.card3}>
          <Text style={styles.title}>Barang</Text>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.gambarproduk}
              source={require('../assetss/image/Sepatu2.png')}
            />
            <View>
              <Text style={styles.merek}>New Balance - Pink Abu - 40</Text>
              <Text style={styles.jasa1}>Cuci Sepatu</Text>
              <Text style={styles.note}>Note:-</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.jumlahitem1}>1 Pasang</Text>
            <Text style={styles.hargaperitem}>@Rp 50.000</Text>
          </View>
        </View>
        <View style={styles.card4}>
          <Text style={styles.title}>Rincian Pembayaran</Text>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.jasa2}>Cuci Sepatu</Text>
              <Text style={styles.jumlahitem2}>x1 Pasang</Text>
              <Text style={styles.hargajasa}>Rp 30.000</Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 2}}>
              <Text style={styles.ongkir}>Biaya Antar</Text>
              <Text style={styles.hargaongkir}>Rp 3.000</Text>
            </View>
            <View style={styles.garis}></View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.total}>Total</Text>
              <Text style={styles.totalbiaya}>Rp 33.000</Text>
            </View>
          </View>
        </View>
        <View style={styles.card5}>
          <Text style={styles.title}>Pilih Pembayaran</Text>
          <View style={{flexDirection: 'row'}}>
            <FlatList
              horizontal={true}
              data={DATA}
              renderItem={({item}) => (
                <Item title={item.title} image={item.image} />
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
        <View style={styles.card6}>
          <TouchableOpacity style={styles.tombolpesan} onPress={()=> navigation.navigate('PaymentScreen')}>
            <Text style={styles.texttombolpesan}>Pesan Sekarang</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  texttombolpesan: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  tombolpesan: {
    width: '90%',
    backgroundColor: '#BB2427',
    height: 55,
    borderRadius: 8,
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 22,
  },
  button1: {
    color: '#000',
    marginTop: 5,
  },
  kreditimg: {
    width: 32,
    height: 32,
  },
  ovoimg: {
    width: 71,
    height: 22,
  },
  transferimg: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
  },
  button: {
    backgroundColor: '#FFFFFF',
    width: 126,
    height: 82,
    borderWidth: 1,
    borderColor: '#E1E1E1',
    marginRight: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  garis: {
    height: 1,
    width: 350,
    alignSelf: 'center',
    backgroundColor: '#EDEDED',
    marginTop: 10,
    marginBottom: 8,
    marginLeft: -26,
  },
  totalbiaya: {
    color: '#034262',
    fontSize: 12,
    fontWeight: '700',
    position: 'absolute',
    right: 22,
  },
  total: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
  },
  hargaongkir: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
    position: 'absolute',
    right: 22,
  },
  ongkir: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
  },
  hargajasa: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
    position: 'absolute',
    right: 22,
  },
  jumlahitem2: {
    color: '#FFC107',
    fontSize: 12,
    fontWeight: '500',
    marginLeft: 15,
  },
  jasa2: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
  },
  hargaperitem: {
    color: '#000',
    fontSize: 16,
    fontWeight: '700',
    marginTop: 16,
    marginLeft: 160,
  },
  jumlahitem1: {
    color: '#000',
    fontSize: 16,
    fontWeight: '400',
    marginTop: 16,
  },
  note: {
    color: '#737373',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 11,
  },
  jasa1: {
    color: '#737373',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 11,
  },
  merek: {
    color: '#000',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 10,
  },
  gambarproduk: {
    height: 84,
    width: 84,
    marginRight: 13,
  },
  alamattoko: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
  },
  namatoko: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 6,
  },
  email: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
  },
  alamat: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 10,
  },
  nama: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 6,
  },
  title: {
    color: '#979797',
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 10,
  },
  card6: {
    height: 98,
    backgroundColor: 'white',
  },
  card5: {
    height: 153,
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingTop: 18,
  },
  card4: {
    height: 124,
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingTop: 18,
  },
  card3: {
    height: 191,
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingTop: 18,
  },
  card2: {
    height: 107,
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingTop: 18,
  },
  card1: {
    height: 153,
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingTop: 18,
  },
  container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
});
