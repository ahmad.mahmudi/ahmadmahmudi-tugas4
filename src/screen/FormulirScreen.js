import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import React from 'react';
import CheckBox from '@react-native-community/checkbox';
import {useState} from 'react';

function Formulir({navigation}) {
  const [centang1, setCentang1] = useState(false);
  const [centang2, setCentang2] = useState(false);
  const [centang3, setCentang3] = useState(false);
  const [centang4, setCentang4] = useState(false);
  return (
    <View style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView>
          <View style={styles.body}>
            <Text style={styles.text}>Merek</Text>
            <TextInput
              placeholder="Masukkan Merek Barang"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Warna</Text>
            <TextInput
              placeholder="Warna Barang, Cth : Merah Putih"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Ukuran</Text>
            <TextInput
              placeholder="Cth : S, M, L / 39,40,41"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Photo</Text>
            <View style={styles.boxgambar}>
              <TouchableOpacity style={styles.btnimg}>
                <Image
                  style={styles.camera}
                  source={require('../assetss/icon/Camera.png')}
                />
                <Text style={styles.add}>Add Photo</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                disabled={false}
                value={centang1}
                onValueChange={newValue => setCentang1(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Ganti Sol Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                disabled={false}
                value={centang2}
                onValueChange={newValue => setCentang2(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Jahit Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                disabled={false}
                value={centang3}
                onValueChange={newValue => setCentang3(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Repaint Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                disabled={false}
                value={centang4}
                onValueChange={newValue => setCentang4(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Cuci Sepatu</Text>
            </View>
            <Text style={styles.text}>Catatan</Text>
            <TextInput
              placeholder="Cth: Ingin ganti sol baru..."
              placeholderTextColor={'#A2A4AB'}
              multiline={true}
              textAlignVertical="top"
              style={styles.input2}></TextInput>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('Keranjang')}>
              <Text style={styles.btntext}>Masukkan Keranjang</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
}

export default Formulir;

const styles = StyleSheet.create({
  checktext: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
  },
  check: {
    width: 24,
    height: 24,
    marginRight: 23,
  },
  checkboxcontainer: {
    flexDirection: 'row',
    marginBottom: 23,
  },
  btntext: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  btn: {
    backgroundColor: '#BB2427',
    height: 55,
    borderRadius: 10,
    justifyContent: 'center',
  },
  add: {
    color: '#BB2427',
    marginTop: 14,
    marginBottom: -18,
    textAlign: 'center',
  },
  btnimg: {
    backgroundColor: 'white',
    width: 84,
    height: 84,
    borderRadius: 8,
    justifyContent: 'center',
  },
  boxgambar: {
    backgroundColor: '#BB2427',
    padding: 1,
    borderRadius: 8,
    width: 86,
    height: 86,
    marginTop: 21,
    marginBottom: 43,
  },
  camera: {
    width: 20,
    height: 18,
    alignSelf: 'center',
  },
  input2: {
    backgroundColor: '#F6F8FF',
    height: 92,
    width: '100%',
    borderRadius: 7,
    marginTop: 11,
    marginBottom: 27,
    paddingLeft: 9,
  },
  input1: {
    backgroundColor: '#F6F8FF',
    height: 45,
    width: '100%',
    borderRadius: 7,
    marginTop: 11,
    marginBottom: 27,
    paddingLeft: 9,
  },
  text: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  container: {
    flex: 1,
  },
  body: {
    width: '100%',
    height: 1000,
    backgroundColor: '#FFF',
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: 30,
  },
});
