import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';

export default function ReserveasiKode({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.info}>
        <Text style={styles.text1}>20 Desember 2020 09:00</Text>
        <Text style={styles.text2}>CS122001</Text>
        <Text style={styles.text3}>Kode Reservasi</Text>
        <Text style={styles.text4}>
          Sebutkan Kode Reservasi saat tiba di outlet
        </Text>
      </View>
      <Text style={styles.title}>Barang</Text>
      <View style={styles.card2}>
        <Image
          style={styles.cardimg}
          source={require('../assetss/image/Sepatu2.png')}
        />
        <View>
          <Text style={styles.cardtext1}>New Balance -Pink Abu - 40</Text>
          <Text style={styles.cardtext2}>Cuci Sepatu</Text>
          <Text style={styles.cardtext2}>Note: -</Text>
        </View>
      </View>
      <Text style={styles.title}>Status Pesanan</Text>
      <TouchableOpacity style={styles.card3} onPress={() => navigation.navigate('Checkout')}>
        <View style={styles.circle}>
          <View style={styles.innercircle}></View>
        </View>
        <View>
          <Text style={styles.text5}>Telah Reservasi</Text>
          <Text style={styles.text6}>20 Desember 2020</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  text6: {
    color: '#A5A5A5',
    fontSize: 10,
    fontWeight: '400'
  },
  text5: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400'
  },
  innercircle: {
    backgroundColor: 'rgba(187, 36, 39, 1)',
    width: 10,
    height: 10,
    borderRadius: 25,
    alignSelf: 'center',
  },
  circle: {
    backgroundColor: 'rgba(187, 36, 39, 0.12)',
    width: 14,
    height: 14,
    borderRadius: 25,
    justifyContent: 'center',
    marginTop: 10,
    marginRight: 14,
  },
  card3: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '95%',
    alignSelf: 'center',
    paddingBottom: 24,
    paddingTop: 20,
    paddingLeft: 20,
    borderRadius: 8,
    marginBottom: 16,
  },
  title: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginTop: 18,
    marginBottom: 16,
    marginLeft: 14,
  },
  text4: {
    width: 238,
    color: '#6F6F6F',
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'center',
    marginTop: 40,
  },
  text3: {
    color: '#000',
    fontSize: 14,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 0,
  },
  text2: {
    color: '#201F26',
    fontSize: 36,
    fontWeight: '700',
    alignSelf: 'center',
    marginTop: 50,
  },
  text1: {
    color: '#BDBDBD',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 16,
    alignSelf: 'center',
  },
  info: {
    backgroundColor: 'white',
    width: '100%',
    height: 240,
  },
  container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  card2: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '95%',
    alignSelf: 'center',
    paddingBottom: 24,
    paddingTop: 20,
    paddingLeft: 20,
    borderRadius: 8,
    marginBottom: 16,
  },
  cardtext2: {
    color: '#737373',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 11,
  },
  cardtext1: {
    color: 'black',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 9,
  },
  cardimg: {
    width: 84,
    height: 84,
    marginRight: 13,
  },
});
