import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import RegisterScreen from './RegisterScreen';
import BottomTabsNavigation from '../BottomTabsNavigation';

const Login = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 10}}>
          <Image
            style={styles.gambar}
            source={require('../assetss/image/Rectangle.png')}
          />
          <View style={styles.konten}>
            <Text style={styles.text}>Welcome,</Text>
            <Text style={styles.text}>Please Login First</Text>
            <Text style={styles.ket}>Email</Text>
            <TextInput
              style={styles.input}
              keyboardType="email"
              placeholder="Youremail@gmail.com"
            />
            <Text style={styles.ket}>Password</Text>
            <TextInput
              style={styles.input}
              keyboardType="password"
              secureTextEntry={true}
              placeholder="Password*****"
            />
            <View style={styles.listicon}>
              <Image
                style={styles.icon}
                source={require('../assetss/icon/googlelogo.png')}
              />
              <Image
                style={styles.icon}
                source={require('../assetss/icon/fblogo.png')}
              />
              <Image
                style={styles.icon}
                source={require('../assetss/icon/twitterlogo.png')}
              />
              <Text style={styles.forgot}>Forgot Password ?</Text>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => navigation.navigate('BottomTabNavigation')}
                style={styles.tombol}>
                <Text style={styles.tomboltext}>Login</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.footerbox}>
              <Text style={styles.footertext1}>
                Don't Have An Account yet?
                <Text
                  style={styles.footertext2}
                  onPress={() => navigation.navigate('RegisterScreen')}>
                  {' '}
                  Register
                </Text>
              </Text>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  gambar: {
    width: Dimensions.get('window').width,
    height: 317,
  },
  konten: {
    width: Dimensions.get('window').width,
    height: 460,
    borderRadius: 15,
    backgroundColor: '#fff',
    padding: 10,
    marginTop: -30,
    paddingTop: 25,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
    fontFamily: 'Monsterrat',
    lineHeight: 30,
    marginLeft: 10,
  },
  input: {
    backgroundColor: '#F6F8FF',
    borderRadius: 8,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10,
    marginRight: 10,
    color: 'black',
    fontSize: 12,
  },
  ket: {
    color: '#BB2427',
    fontWeight: 'bold',
    fontSize: 12,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
  },
  listicon: {
    flexDirection: 'row',
    marginLeft: 8,
  },
  icon: {
    width: 30,
    height: 27,
    margin: 9,
    marginTop: 18,
  },
  forgot: {
    padding: 10,
    margin: 10,
    textAlign: 'center',
    width: 280,
    fontSize: 10,
  },
  tombol: {
    backgroundColor: '#BB2427',
    borderRadius: 8,
    marginTop: 50,
    marginLeft: 10,
    marginRight: 10,
  },
  tomboltext: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
    padding: 10,
  },
  footertext1: {
    color: '#717171',
    fontSize: 12,
  },
  footertext2: {
    color: '#BB2427',
  },
  footerbox: {
    position: 'absolute',
    bottom: 15,
    left: '25%',
  },
});
