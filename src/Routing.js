import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import SplashScreen from './screen/SplashScreen';
import AuthNavigation from './AuthNavigation';
import BottomTabsNavigation from './BottomTabsNavigation';
import HomeNav from './HomeNavigation'
import TransactionNavigation from './TransactionNavigation';
import ProfileNavigation from './ProfileNavigation';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name='SplashScreen' component={SplashScreen}/>
            <Stack.Screen name='AuthNavigation' component={AuthNavigation}/>
            <Stack.Screen name='BottomTabNavigation' component={BottomTabsNavigation}/>
            <Stack.Screen name='HomeNavigation' component={HomeNav}/>
            <Stack.Screen name='TransactionsNavigation' component={TransactionNavigation}/>
            <Stack.Screen name='ProfileNavigation' component={ProfileNavigation}/>
        </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({})