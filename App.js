import { StatusBar, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import Routing from './src/Routing'

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle={'dark-content'}></StatusBar>
      <Routing />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,},
})